package titulaciones;

public class Asignatura {
	
	private String nombre;
	private int horas;
	private int temas;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getHoras() {
		return horas;
	}
	public void setHoras(int horas) {
		this.horas = horas;
	}
	public int getTemas() {
		return temas;
	}
	public void setTemas(int temas) {
		this.temas = temas;
	}

}
