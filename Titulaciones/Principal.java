package titulaciones;

import org.hibernate.Session;

public class Principal {

	public static void main(String[] args) {
		
		Session session = HibernateUtilities.getSessionFactory().openSession();	
		
		session.close();
		
		HibernateUtilities.getSessionFactory().close();
		
		}
		
		public static void IntroducirDatos(Session session) {
			
			/*A�adimos la primera titulaci�n*/

			session.beginTransaction();

			Titulacion t1 = new Titulacion();
			t1.setNombre("Desarrollo de Aplicaciones Multiplataforma");
			t1.setAbreviatura("DAM");
			t1.setHoras(2000);
			
			Asignatura a1 = new Asignatura();
			a1.setNombre("Acceso a Datos");
			a1.setHoras(120);
			a1.setTemas(8);
			
			Profesor p1 = new Profesor();
			p1.setNombre("Juanmi");
			p1.setHorario("Miercoles y Viernes");

			Asignatura a2 = new Asignatura();
			a2.setNombre("Desarrollo de Interfaces");
			a2.setHoras(120);
			a2.setTemas(7);
			
			Profesor p2 = new Profesor();
			p2.setNombre("Tom�s");
			p2.setHorario("Martes y Viernes");

			/* TODO:
			 * A�adir a1 y a2 en la titulaci�n t1
			 * Enlazar a1 con p1 y a2 con p2
			 */
			
			session.save(t1);
			
			session.getTransaction().commit();
			
			/*A�adimos la segunda titulaci�n*/

			session.beginTransaction();

			t1 = new Titulacion();
			t1.setNombre("Desarrollo de Aplicaciones Web");
			t1.setAbreviatura("DAW");
			t1.setHoras(2000);
			
			a1 = new Asignatura();
			a1.setNombre("Sistemas Inform�ticos");
			a1.setHoras(160);
			a1.setTemas(10);
			
			p1 = new Profesor();
			p1.setNombre("Juanmi");
			p1.setHorario("Lunes y Viernes");

			a2 = new Asignatura();
			a2.setNombre("Desarrollo web en entorno Servidor");
			a2.setHoras(100);
			a2.setTemas(6);
			
			p2 = new Profesor();
			p2.setNombre("Paco");
			p2.setHorario("Martes y Jueves");

			/* TODO:
			 * A�adir a1 y a2 en la titulaci�n t1
			 * Enlazar a1 con p1 y a2 con p2
			 */
			
			session.save(t1);
			
			session.getTransaction().commit();		

		}

}
